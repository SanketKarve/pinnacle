# README

## How to use it:
* Clone the repository
* Install bundles

```
 bundle install
```

* Setup Database

```
  rails db:setup
  rails db:migrate
```

* Install webpacker

```
  bundle exec rails webpacker:install
```

* Start the Server:

  Starting Rails Server

```
  rails s
```
  Starting webpack dev server on other terminal

```
  ./bin/webpack-dev-server
```

## Reference website:
- https://github.com/codahale/bcrypt-ruby
- https://github.com/rails/webpacker
- https://vuejs.org/v2/guide/index.html
- https://getbootstrap.com/docs/4.0/getting-started/introduction/
