class ArticlesController < ApplicationController

  before_action :authorize

  def index
    @articles = Article.all
  end

  def new
  end

  def create
    @article = Article.new(article_params)
    @article.user = current_user

    if @article.save
      redirect_to '/index'
    else
      render 'new'
    end
  end

  def show
    @article = Article.find(params[:id])
  end

  private
  def article_params
    params.permit(:name, :document)
  end
end
