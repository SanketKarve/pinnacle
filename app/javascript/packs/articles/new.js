import Vue from 'vue/dist/vue.js';
import ArticlesNewView from 'views/articles/new';
import BootstrapVue from 'bootstrap-vue'
require('froala-editor/js/froala_editor.pkgd.min')

import VueFroala from 'vue-froala-wysiwyg'
Vue.use(VueFroala)
Vue.use(BootstrapVue);
new Vue({
    el: '#articles-new-view',
    components: {
        'articles-new-view': ArticlesNewView
    }
});
