import Vue from 'vue/dist/vue.js';
import ArticlesShowView from 'views/articles/show';
import BootstrapVue from 'bootstrap-vue'
require('froala-editor/js/froala_editor.pkgd.min')

import VueFroala from 'vue-froala-wysiwyg'
Vue.use(VueFroala)
Vue.use(BootstrapVue);
new Vue({
    el: '#articles-show-view',
    components: {
        'articles-show-view': ArticlesShowView
    }
});
